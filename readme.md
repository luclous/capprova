Na sua interpretação:
a. Qual foi o nível de dificuldade da implementação da questão do bloco A?
[ ] Muito Fácil
[ ] Fácil
[X] Médio
[X] Difícil
[ ] Muito Difícil
b. Qual foi o nível de dificuldade para encontrar os erros do programa da questão 7?
[ ] Muito Fácil
[ ] Fácil
[X] Médio
[ ] Difícil
[ ] Muito Difícil